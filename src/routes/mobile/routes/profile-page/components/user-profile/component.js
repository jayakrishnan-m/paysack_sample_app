// let data = require('./data');
let userprofile = require('./userprofile').getProfile;

module.exports = class {
  onCreate() {

    this.state = {
      profile: []
    };
  }
  onMount() {


    /*******Listen to View events*****/

    /*
    Docs - http://framework7.io/docs/page.html#page-events
    */

    Dom7(document).on('page:reinit', '.page[data-name="profile"]', (e) => {
      this.profile();
    })
  }

  profile() {
    console.log("Entered profile()");
    userprofile().then(
      res => {
        let profile = [{
            name: 'Name',
            value: `${res.first_name} ${res.last_name}`
          },
          {
            name: 'Phone',
            value: res.mobile
          },
          {
            name: 'Email',
            value: res.email
          },
          {
            name: 'Gender',
            value: res.gender || ''
          },
          {
            name: 'Birthday',
            value: res.kit && res.kit.specialDate || ''
          },
          {
            name: 'City,State',
            value: `${res.kit && res.kit.city || ''} ${res.kit && res.kit.state || ''}`
          },
          {
            name: 'Id Type',
            value: res.kit && res.kit.idType || ''
          },
          {
            name: 'Id Number',
            value: res.kit && res.kit.idNumber || ''
          },
          {
            name: 'kit Number',
            value: res.kit && res.kit.kitNo || ''
          }
        ];
        console.log(profile);
        this.state.profile = profile;
      },
      err => {}
    );
  }
};
