import { ModelFactory, MODEL_PATHS } from 'pk-client';
import { Base_Url } from '../../../../../../environment';

export async function getProfile(): Promise<any> {
  let token = window.sessionStorage.getItem('auth_token');
  let mf = new ModelFactory({ api_base_url: Base_Url, token: token });
  let result, kit_no;
  try {
    result = await mf
      .anyModel(MODEL_PATHS.employee_profile)
      .get(await getUserId());
    console.log(result);
    if (result.error) throw result.error;
    kit_no = (result.kit && result.kit.kitNo) || '';
    window.sessionStorage.setItem(
      'kit_no',
      kit_no
    ); /* Save Kit Number in localstorage*/
    return result;
  } catch (e) {
    console.log(e.toString());
    console.log(e.response.status);
  }
}

export async function getUserId(): Promise<any> {
  let token = window.sessionStorage.getItem('auth_token');
  let mf = new ModelFactory({ api_base_url: Base_Url, token: token });
  let result;
  try {
    result = await mf.anyModel(MODEL_PATHS.employees).get('me');
    console.log(result._id);
    if (result.error) throw result.error;
    return result._id;
  } catch (e) {
    console.log(e.toString());
    console.log(e.response.status);
  }
}
