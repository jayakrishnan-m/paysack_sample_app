"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pk_client_1 = require("pk-client");
const environment_1 = require("../../../../../../environment");
async function getProfile() {
    let token = window.sessionStorage.getItem('auth_token');
    let mf = new pk_client_1.ModelFactory({ api_base_url: environment_1.Base_Url, token: token });
    let result, kit_no;
    try {
        result = await mf
            .anyModel(pk_client_1.MODEL_PATHS.employee_profile)
            .get(await getUserId());
        console.log(result);
        if (result.error)
            throw result.error;
        kit_no = (result.kit && result.kit.kitNo) || '';
        window.sessionStorage.setItem('kit_no', kit_no); /* Save Kit Number in localstorage*/
        return result;
    }
    catch (e) {
        console.log(e.toString());
        console.log(e.response.status);
    }
}
exports.getProfile = getProfile;
async function getUserId() {
    let token = window.sessionStorage.getItem('auth_token');
    let mf = new pk_client_1.ModelFactory({ api_base_url: environment_1.Base_Url, token: token });
    let result;
    try {
        result = await mf.anyModel(pk_client_1.MODEL_PATHS.employees).get('me');
        console.log(result._id);
        if (result.error)
            throw result.error;
        return result._id;
    }
    catch (e) {
        console.log(e.toString());
        console.log(e.response.status);
    }
}
exports.getUserId = getUserId;
