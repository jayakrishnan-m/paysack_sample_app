let login = require('./login').login;

module.exports = class {

  onCreate() {
    this.state = {
      emailError: null,
      passwordError: null,
      formErrors: true,
      submitting: false,
      displayMessage: ''
    }
  }

  onMount() {
    this.initForm();
    this.checkErrors();
  }

  signIn() {
    console.log('Entered signIn()')
    console.log(this.email.value, this.password.value);
    this.state.submitting = true;
    this.state.displayMessage = 'Please wait...';
    console.log("Calling Login()")
    login(this.email.value, this.password.value)
      .then(
        res => {
          window.app.views.main.router.navigate({name:"profile"});
          this.state.displayMessage = 'Login Success';
        },
        err => {
          this.state.displayMessage = 'Invalid details! Please try again.';
          this.resetForm();
        }  
      );
  }

  initForm() {
    this.loginForm = this.getEl('loginForm');
    this.email = this.getEl('username');
    this.password = this.getEl('password');
    this.loginButton = this.getEl('loginButton');
  }

  checkErrors() {
    this.email.addEventListener("input", () => {
      this.state.emailError = this.email.validity.valid ? '' : 'Please enter a valid email!';
      this.state.formErrors = this.isValid() ? false : true;
    }, false);

    this.password.addEventListener("input", () => {
      this.state.passwordError = this.password.validity.valid ? '' : 'Password is required.';
      this.state.formErrors = this.isValid() ? false : true;
    }, false);
  }

  isValid() {
    return (this.email.validity.valid && this.password.validity.valid) ? true : false;
  }

  resetForm() {
    this.loginForm.reset();
    this.state.submitting = false;
    this.state.formErrors = true;
  }

}
