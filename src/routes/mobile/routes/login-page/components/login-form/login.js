"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pk_client_1 = require("pk-client");
const environment_1 = require("../../../../../../environment");
async function login(username, password) {
    console.log("Entered here");
    let sc = new pk_client_1.SessionChallenge(environment_1.Base_Url, username, password);
    console.log(environment_1.Base_Url, username, password);
    return await sc.login()
        .then(response => {
        console.log(response);
        if (response['token']) {
            window.sessionStorage.setItem('auth_token', response['token']);
            return true;
        }
        else {
            throw response;
        }
    }, err => {
        throw err;
    });
}
exports.login = login;
