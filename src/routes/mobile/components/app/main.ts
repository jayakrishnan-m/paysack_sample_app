import NativeBridge from "./src/NativeBridge";
import NotificationCenter from "./src/NotificationCenter";
import BackPressHandler from "./src/android/BackPressHandler";
interface CommandProtocol {
    execute(info: any, options?: any): void
}
declare var fireEvent: any
declare var Framework7: any
var isAndroid = Framework7.device.android === true;
var isIos = Framework7.device.ios === true;
let eventHandler: { [key: string]: CommandProtocol } = {

}

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function urldecodeAndroidArgs(args: any) {
    for (var key in args) {
        var value = args[key];
        args[key] = typeof value == 'string' ? urldecode(value) : urldecodeAndroidArgs(value)
    }
    return args;
}

(<any>window).fireEvent = function (event: string, args: any) {
    args = isAndroid ? urldecodeAndroidArgs(args) : args;
    eventHandler[event] ? eventHandler[event].execute(args) : null
    NotificationCenter.sharedInstance.emit(event, args)
    return { ok: "true" }
}
document.addEventListener("DOMContentLoaded", function (event) {
    NativeBridge.sendMessage("loaded", {})
    NativeBridge.setupNativeBridge()
});

isAndroid && BackPressHandler.listen();
