import * as _ from "lodash"

export default class CommonUtil {
    static removeNulls(obj: any): any {
        return obj instanceof Array ? CommonUtil.removeNullsFromArray(obj) : CommonUtil.removeNullsFromObject(obj)
    }
    private static removeNullsFromArray(obj: any[]): any[] {

        let map = _.map(obj, (item) => {
            return CommonUtil.removeNulls(item);
        })
        return map;
    }

    private static removeNullsFromObject(obj: any): any {
        let copy = _.cloneDeep(obj);
        let nullKeys = [];
        for (let key in copy) {
            copy[key] === null && nullKeys.push(key);
            copy[key] = copy[key] instanceof Array ? CommonUtil.removeNullsFromArray(copy[key]) : copy[key];
            copy[key] = typeof copy[key] === "object" && copy[key] !== null ? CommonUtil.removeNullsFromObject(copy[key]) : copy[key];
        }
        for (let key of nullKeys) {
            delete copy[key];
        }
        return copy;
    }
}