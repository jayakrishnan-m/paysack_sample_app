"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
class CommonUtil {
    static removeNulls(obj) {
        return obj instanceof Array ? CommonUtil.removeNullsFromArray(obj) : CommonUtil.removeNullsFromObject(obj);
    }
    static removeNullsFromArray(obj) {
        let map = _.map(obj, (item) => {
            return CommonUtil.removeNulls(item);
        });
        return map;
    }
    static removeNullsFromObject(obj) {
        let copy = _.cloneDeep(obj);
        let nullKeys = [];
        for (let key in copy) {
            copy[key] === null && nullKeys.push(key);
            copy[key] = copy[key] instanceof Array ? CommonUtil.removeNullsFromArray(copy[key]) : copy[key];
            copy[key] = typeof copy[key] === "object" && copy[key] !== null ? CommonUtil.removeNullsFromObject(copy[key]) : copy[key];
        }
        for (let key of nullKeys) {
            delete copy[key];
        }
        return copy;
    }
}
exports.default = CommonUtil;
