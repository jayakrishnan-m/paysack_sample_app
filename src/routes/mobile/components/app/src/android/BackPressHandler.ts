import NativeBridge from "../NativeBridge";
import NotificationCenter from "../NotificationCenter";

declare var myApp: any
declare var Dom7: any
declare var window: any

export default class BackPressHandler {

    static listen() {
        NotificationCenter.sharedInstance.on("BACK_PRESSED", () => {
            console.log("Got back press from shell")
            window.app.views.main.router.back();
        });
    }

    private static isNonDissmissableDialogPresent() {
        return Dom7('.modal-preloader').length > 0;
    }

    private static handleBack() {
        this.isPopupOpen() ? this.closePopups() : this.closeNonPopupViews();
    }

    private static getOpenPopups() {
        let $popup = Dom7('.dismissable-popup.modal-in,.actions-modal.modal-in')
        return $popup;
    };

    private static closeNonPopupViews() {
        this.isPanelOpen() ? this.closePanel() : this.popView()
    };

    private static closePanel() {
        // Analytics.trackScreenView("dashboard-index")
        myApp.closePanel();
    };

    private static isPanelOpen() {
        return Dom7('body').hasClass('with-panel-left-cover');
    };

    private static popView() {
        this.hasHistory() ? this.goBack() : this.askShellToHandleBackPress()
    };

    private static closePopups() {
        var $popups = this.getOpenPopups();
        myApp.closeModal($popups)
    }

    private static isPopupOpen() {
        var $popup = this.getOpenPopups();
        return $popup.length > 0
    }

    private static hasHistory() {
        return window.mainView.history.length > 1;
    }

    private static goBack() {
        window.mainView.router.back();
    }

    private static askShellToHandleBackPress() {
        return NativeBridge.sendMessage("back");
    }
}