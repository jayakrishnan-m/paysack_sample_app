import NotificationCenter from "./NotificationCenter";
import CommonUtil from './CommonUtil';
declare var Framework7: any
declare var window: any
declare var console: any

export interface NativePluginCall {
    type: string
    action: string
    _id?: string
    params: {}
}


class NativeBridge {

    static plugins = {
        calendar: {
            type: "calendar",
            addSlot: "addSlot",
            removeSlot: "removeSlot"
        },
        auth: {
            type: "auth",
            login: "login",
            deviceToken: "deviceToken"
        }
    }
    static pendingPromises: { [key: string]: { resolve: Function, reject: Function } } = {}
    static setupNativeBridge() {
        NotificationCenter.sharedInstance.removeAllListeners("nativeResponse");
        NotificationCenter.sharedInstance.on("nativeResponse", (resp) => {


            let id: string = resp._id;
            let data = resp.data || {}
            id && NativeBridge.pendingPromises[id] && (data.success ? NativeBridge.pendingPromises[id].resolve(resp) : NativeBridge.pendingPromises[id].reject(resp));
            delete NativeBridge.pendingPromises[id]
        })
    }
    static pluginApiCall(params: NativePluginCall): Promise<any> {
        params.params = Framework7.device.ios ? CommonUtil.removeNulls(params.params) : params.params; //ios does not allow null to be stored in localNotifaction data
        let reqID = `${params.type}-${new Date().getTime()}`
        params._id = reqID;
        let prom = new Promise((resolve, reject) => {
            NativeBridge.pendingPromises[reqID] = { resolve: resolve, reject: reject };
            NativeBridge.sendMessage("pluginApiCall", params);
        })

        return prom;
    }
    static sendMessage(name: string, object?: any) {
        console.log("Sending bridge message" + ":" + name + ":" + JSON.stringify(object))
        if (Framework7.device.ios && (window as any).webkit) {
            try {
                let handler = window.webkit.messageHandlers && window.webkit.messageHandlers[name]
                handler && (typeof handler.postMessage === "function") ? handler.postMessage(object) : null;
            } catch (e) {
            }

        } else if ((window as any).shellBridge) {
            var param = object;
            if (object !== null && typeof object === 'object') {
                param = JSON.stringify(object);
            }
            (window as any).shellBridge.onJavascriptEvent(name, param);
        }
    }
}

export default NativeBridge

//Read cordova's implementation from native to js
// https://github.com/apache/cordova-ios/blob/f75bf67438257132e739238ed579d73271b3a716/CordovaLib/Classes/Public/CDVCommandDelegateImpl.m