let routes = [
  {
    name: 'root',
    path: '/',
    pageName: 'login'
  },
  {
    name: 'home',
    path: '/home',
    pageName: 'home'
  },
  {
    name: 'login',
    path: '/login',
    pageName: 'login'
  },
  {
    name: 'profile',
    path: '/profile',
    pageName: 'profile'
  }
];

exports.routes = routes;
