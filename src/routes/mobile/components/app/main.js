"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NativeBridge_1 = require("./src/NativeBridge");
const NotificationCenter_1 = require("./src/NotificationCenter");
const BackPressHandler_1 = require("./src/android/BackPressHandler");
var isAndroid = Framework7.device.android === true;
var isIos = Framework7.device.ios === true;
let eventHandler = {};
function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function urldecodeAndroidArgs(args) {
    for (var key in args) {
        var value = args[key];
        args[key] = typeof value == 'string' ? urldecode(value) : urldecodeAndroidArgs(value);
    }
    return args;
}
window.fireEvent = function (event, args) {
    args = isAndroid ? urldecodeAndroidArgs(args) : args;
    eventHandler[event] ? eventHandler[event].execute(args) : null;
    NotificationCenter_1.default.sharedInstance.emit(event, args);
    return { ok: "true" };
};
document.addEventListener("DOMContentLoaded", function (event) {
    NativeBridge_1.default.sendMessage("loaded", {});
    NativeBridge_1.default.setupNativeBridge();
});
isAndroid && BackPressHandler_1.default.listen();
